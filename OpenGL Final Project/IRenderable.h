#pragma once
#ifndef _I_RENDERABLE_H_
#define _I_RENDERABLE_H_
struct DirectionalLight;
struct PointLight;
struct SpotLight;
class Shader;
class IRenderable
{
	friend class RenderSystem;
	friend class Engine;
protected:
	Shader* mMainShader = nullptr;
	std::map<unsigned int, Shader*> shaders;
	std::list<PointLight*> pointLights;
	std::list<SpotLight*> spotLights;
protected:
	IRenderable();
	~IRenderable();

	virtual void render() = 0;
	void setLights(const DirectionalLight*, const std::list<PointLight*>&,const std::list<SpotLight*>&);
	virtual void initialize(json::JSON&);
	virtual void update(float);
};

#endif