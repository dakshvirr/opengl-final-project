#pragma once
#ifndef _I_SYSTEM_H_
#define _I_SYSTEM_H_
class ISystem
{
	friend class Engine;
protected:
	bool initialized = false;
	virtual void initialize(json::JSON&) = 0;
	virtual void update(float) = 0;
public:
	inline bool isInitialized() { return initialized; }
};

#endif