#pragma once
#include "IRenderable.h"
class Model;
class SceneModel : public IRenderable
{
	friend class Engine;
	glm::mat4 mModel;
	glm::vec3 Position;
	glm::vec3 FRONT;
	glm::vec3 RIGHT;
	glm::vec3 UP;
	Model* model;
	float speed;
	float shininess;
protected:
	SceneModel();
	~SceneModel();
	// Inherited via IRenderable
	virtual void render() override;
	virtual void initialize(json::JSON&) override;
	virtual void update(float deltaTime) override;
private:
	void HandleInput(float deltaTime);
};

