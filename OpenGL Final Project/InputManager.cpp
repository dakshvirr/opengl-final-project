#include "Core.h"
#include "Camera.h"
#include "RenderSystem.h"
#include "InputManager.h"

InputManager::~InputManager()
{
	if (mCamera != nullptr)
	{
		delete mCamera;
		mCamera = nullptr;
	}
}

void InputManager::getMouseEvents(GLFWwindow* window, double xpos, double ypos)
{
	if (InputManager::instance().mCamera == nullptr)
	{
		return;
	}
	if (InputManager::instance().mFirstTime)
	{
		InputManager::instance().mFirstTime = false;
		InputManager::instance().lastMouseX = xpos;
		InputManager::instance().lastMouseY = ypos;
	}

	float xoffset = xpos - InputManager::instance().lastMouseX;
	float yoffset = InputManager::instance().lastMouseY - ypos;

	InputManager::instance().lastMouseX = xpos;
	InputManager::instance().lastMouseY = ypos;

	InputManager::instance().mCamera->ProcessMouseMovement(xoffset, yoffset);
}

void InputManager::getScrollEvents(GLFWwindow* window, double xoffset, double yoffset)
{
	if (InputManager::instance().mCamera == nullptr)
	{
		return;
	}
	InputManager::instance().mCamera->ProcessMouseScroll(yoffset);
}

const glm::mat4& InputManager::getViewMatrix()
{
	if (mCamera == nullptr)
	{
		return glm::mat4();
	}
	else
	{
		return mCamera->GetViewMatrix();
	}
}

const glm::mat4& InputManager::getProjectionMatrix()
{
	if (mCamera == nullptr)
	{
		return glm::mat4();
	}
	else
	{
		return mCamera->GetProjectionMatrix();
	}
}

bool InputManager::getInputState(unsigned int keyCode)
{
	if (RenderSystem::instance().getWindow() == nullptr)
	{
		return false;
	}
	if (inputEvents.count(keyCode) == 0)
	{
		inputEvents.emplace(keyCode, glfwGetKey(RenderSystem::instance().getWindow(), keyCode) == GLFW_PRESS);
	}
	return inputEvents[keyCode];
}

void InputManager::initialize(json::JSON& inputNode)
{
	if (inputNode.hasKey("CameraPosition"))
	{
		mCamera = new Camera(glm::vec3(inputNode["CameraPosition"]["X"].ToFloat(), inputNode["CameraPosition"]["Y"].ToFloat(), inputNode["CameraPosition"]["Z"].ToFloat()));
	}
	else
	{
		mCamera = new Camera(glm::vec3(0, 0, 3));
	}
	glfwSetCursorPosCallback(RenderSystem::instance().getWindow(), &InputManager::getMouseEvents);
	glfwSetScrollCallback(RenderSystem::instance().getWindow(), &InputManager::getScrollEvents);
}

void InputManager::update(float deltaTime)
{
	if (RenderSystem::instance().getWindow() != nullptr)
	{
		if (glfwGetKey(RenderSystem::instance().getWindow(), GLFW_KEY_ESCAPE) == GLFW_PRESS)
		{
			glfwSetWindowShouldClose(RenderSystem::instance().getWindow(), true);
			return;
		}
		for (auto& inputEvent : inputEvents)
		{
			inputEvent.second = glfwGetKey(RenderSystem::instance().getWindow(), inputEvent.first) == GLFW_PRESS;
		}
		if (mCamera != nullptr)
		{
			mCamera->update(deltaTime);
		}
		glfwPollEvents();
	}
}
