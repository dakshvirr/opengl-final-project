#include "Core.h"
#include "Shader.h"
#include "InputManager.h"
#include "RenderSystem.h"
#include "IRenderable.h"

IRenderable::IRenderable()
{
}

IRenderable::~IRenderable()
{
}

void IRenderable::setLights(const DirectionalLight* pDLight, const std::list<PointLight*>& pPLights, const std::list<SpotLight*>& pSLights)
{
	mMainShader->use();
	mMainShader->setVec3("directionalLight.lightDir", pDLight->lightDir);
	mMainShader->setVec3("directionalLight.diffuse", pDLight->diffuse);
	mMainShader->setVec3("directionalLight.ambient", pDLight->ambient);
	mMainShader->setVec3("directionalLight.specular", pDLight->specular);
	mMainShader->setFloat("directionalLight.intensity", pDLight->intensity);
	int aI = 0;
	mMainShader->setInt("noOfPointLights", pPLights.size());
	mMainShader->setInt("noOfSpotLights", pSLights.size());
	for (auto& pointLight : pPLights)
	{
		mMainShader->setFloat("pointLights[" + std::to_string(aI) + "].constant", pointLight->constant);
		mMainShader->setFloat("pointLights[" + std::to_string(aI) + "].intensity", pointLight->intensity);
		mMainShader->setFloat("pointLights[" + std::to_string(aI) + "].linear", pointLight->linear);
		mMainShader->setFloat("pointLights[" + std::to_string(aI) + "].quadratic", pointLight->quadratic);
		mMainShader->setVec3("pointLights[" + std::to_string(aI) + "].ambient", pointLight->ambient);
		mMainShader->setVec3("pointLights[" + std::to_string(aI) + "].diffuse", pointLight->diffuse);
		mMainShader->setVec3("pointLights[" + std::to_string(aI) + "].specular", pointLight->specular);
		mMainShader->setVec3("pointLights[" + std::to_string(aI) + "].lightPos", pointLight->lightPos);
		aI++;
	}
	aI = 0;

	for (auto& spotLight : pSLights)
	{
		mMainShader->setFloat("spotLights[" + std::to_string(aI) + "].constant", spotLight->constant);
		mMainShader->setFloat("spotLights[" + std::to_string(aI) + "].intensity", spotLight->intensity);
		mMainShader->setFloat("spotLights[" + std::to_string(aI) + "].linear", spotLight->linear);
		mMainShader->setFloat("spotLights[" + std::to_string(aI) + "].quadratic", spotLight->quadratic);
		mMainShader->setFloat("spotLights[" + std::to_string(aI) + "].cutOff", spotLight->cutOff);
		mMainShader->setFloat("spotLights[" + std::to_string(aI) + "].outerCutOff", spotLight->outerCutOff);
		mMainShader->setVec3("spotLights[" + std::to_string(aI) + "].ambient", spotLight->ambient);
		mMainShader->setVec3("spotLights[" + std::to_string(aI) + "].diffuse", spotLight->diffuse);
		mMainShader->setVec3("spotLights[" + std::to_string(aI) + "].specular", spotLight->specular);
		mMainShader->setVec3("spotLights[" + std::to_string(aI) + "].lightPos", spotLight->lightPos);
		mMainShader->setVec3("spotLights[" + std::to_string(aI) + "].lightDir", spotLight->lightDir);
		aI++;
	}
}

void IRenderable::update(float)
{
	for (auto& shader : shaders)
	{
		if (InputManager::instance().getInputState(shader.first))
		{
			mMainShader = shader.second;
		}
	}
}

void IRenderable::initialize(json::JSON& pNode)
{
	for (auto& shader : pNode["Shaders"].ArrayRange())
	{
		Shader* curShader = nullptr;
		if (shader.hasKey("Geometry"))
		{
			curShader = new Shader(shader["Vertex"].ToString().c_str(), shader["Fragment"].ToString().c_str(), shader["Geometry"].ToString().c_str());
		}
		else
		{
			curShader = new Shader(shader["Vertex"].ToString().c_str(), shader["Fragment"].ToString().c_str());
		}
		shaders.emplace(shader["KeyCode"].ToInt(), curShader);
	}
	mMainShader = shaders[pNode["MainShader"].ToInt()];

	if (pNode.hasKey("PointLights"))
	{
		for (auto& aPointLight : pNode["PointLights"].ArrayRange())
		{
			PointLight* aPLight = new PointLight();
			aPLight->lightPos = glm::vec3(aPointLight["lightPos"]["X"].ToFloat(), aPointLight["lightPos"]["Y"].ToFloat(), aPointLight["lightPos"]["Z"].ToFloat());
			aPLight->constant = aPointLight["constant"].ToFloat();
			aPLight->linear = aPointLight["linear"].ToFloat();
			aPLight->quadratic = aPointLight["quadratic"].ToFloat();
			aPLight->diffuse = glm::vec3(aPointLight["diffuse"]["X"].ToFloat(), aPointLight["diffuse"]["Y"].ToFloat(), aPointLight["diffuse"]["Z"].ToFloat());
			aPLight->ambient = glm::vec3(aPointLight["ambient"]["X"].ToFloat(), aPointLight["ambient"]["Y"].ToFloat(), aPointLight["ambient"]["Z"].ToFloat());
			aPLight->specular = glm::vec3(aPointLight["specular"]["X"].ToFloat(), aPointLight["specular"]["Y"].ToFloat(), aPointLight["specular"]["Z"].ToFloat());
			aPLight->intensity = aPointLight["intensity"].ToFloat();
			RenderSystem::instance().addPointLight(aPLight);
			pointLights.push_back(aPLight);
		}
	}

	if (pNode.hasKey("SpotLights"))
	{
		for (auto& aSpotLight : pNode["SpotLights"].ArrayRange())
		{
			SpotLight* aSLight = new SpotLight();
			aSLight->lightPos = glm::vec3(aSpotLight["lightPos"]["X"].ToFloat(), aSpotLight["lightPos"]["Y"].ToFloat(), aSpotLight["lightPos"]["Z"].ToFloat());
			aSLight->constant = aSpotLight["constant"].ToFloat();
			aSLight->linear = aSpotLight["linear"].ToFloat();
			aSLight->quadratic = aSpotLight["quadratic"].ToFloat();
			float aTemp = aSpotLight["cutOff"].ToFloat();
			aSLight->cutOff = glm::cos(glm::radians(aTemp));
			aTemp = aSpotLight["outerCutOff"].ToFloat();
			aSLight->outerCutOff = glm::cos(glm::radians(aTemp));
			aSLight->diffuse = glm::vec3(aSpotLight["diffuse"]["X"].ToFloat(), aSpotLight["diffuse"]["Y"].ToFloat(), aSpotLight["diffuse"]["Z"].ToFloat());
			aSLight->ambient = glm::vec3(aSpotLight["ambient"]["X"].ToFloat(), aSpotLight["ambient"]["Y"].ToFloat(), aSpotLight["ambient"]["Z"].ToFloat());
			aSLight->specular = glm::vec3(aSpotLight["specular"]["X"].ToFloat(), aSpotLight["specular"]["Y"].ToFloat(), aSpotLight["specular"]["Z"].ToFloat());
			aSLight->intensity = aSpotLight["intensity"].ToFloat();
			RenderSystem::instance().addSpotLight(aSLight);
			spotLights.push_back(aSLight);
		}
	}

}
