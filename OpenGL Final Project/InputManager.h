#pragma once
#ifndef _INPUT_MANAGER_H_
#define _INPUT_MANAGER_H_
#include "ISystem.h"
class Camera;
class InputManager : public ISystem
{
	friend class Engine;
	std::map<unsigned int, bool> inputEvents;
	bool mFirstTime = true;
	Camera* mCamera = nullptr;
	float lastMouseX = -1.0f;
	float lastMouseY = -1.0f;
private:
	InputManager() = default;
	~InputManager();
	InputManager(const InputManager&) = default;
	InputManager& operator= (const InputManager&) = default;

public:
	static InputManager& instance()
	{
		static InputManager _instance;
		return _instance;
	}
	bool getInputState(unsigned int);

	static void getMouseEvents(GLFWwindow* window, double xpos, double ypos);
	static void getScrollEvents(GLFWwindow* window, double xoffset, double yoffset);

	const glm::mat4& getViewMatrix();
	const glm::mat4& getProjectionMatrix();

protected:
	// Inherited via ISystem
	virtual void initialize(json::JSON&) override;
	virtual void update(float deltaTime) override;
};

#endif