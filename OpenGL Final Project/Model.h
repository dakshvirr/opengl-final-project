#pragma once
#ifndef _MODEL_H_
#define _MODEL_H_
#include "Mesh.h"
class Model
{
	friend class InstanceModel;
protected:
	std::vector<Mesh> meshes;
	std::string directory;
	std::vector<Texture> texturesLoaded;
	bool gammaCorrection;
public:
	Model(bool gamma = false);
	bool loadModel(std::string const& path);
	void Draw(Shader* const shader);
private:
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
};

#endif