#include "Core.h"
#include "RenderSystem.h"
#include "InputManager.h"
#include "SceneModel.h"
#include "OpacityModel.h"
#include "InstanceModel.h"
#include "Skybox.h"
#include "Engine.h"


Engine::~Engine()
{
	cleanup();
}

void Engine::cleanup()
{

	mMainSkybox = nullptr;
	for (auto i = mSkyboxes.begin(); i != mSkyboxes.end(); ++i)
	{
		if (*i != nullptr)
		{
			delete *i;
			*i = nullptr;
		}
	}
	mSkyboxes.clear();
	if (mSceneModel != nullptr)
	{
		delete mSceneModel;
		mSceneModel = nullptr;
	}
	for (auto& aOModel : mOpacityModels)
	{
		if (aOModel != nullptr)
		{
			delete aOModel;
			aOModel = nullptr;
		}
	}
	mOpacityModels.clear();
	if (mInstanceModel != nullptr)
	{
		delete mInstanceModel;
		mInstanceModel = nullptr;
	}
}

void Engine::initialize(std::string sceneDataPath)
{
	std::ifstream inputStream(sceneDataPath);
	std::string JSONstr((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON gameNode = json::JSON::Load(JSONstr);
	json::JSON defaultEmpty = json::JSON::Object();
	if (gameNode.hasKey("RenderSettings"))
	{
		RenderSystem::instance().initialize(gameNode["RenderSettings"]);
	}
	else
	{
		RenderSystem::instance().initialize(defaultEmpty);
	}
	if (gameNode.hasKey("InputSettings"))
	{
		InputManager::instance().initialize(gameNode["InputSettings"]);
	}
	else
	{
		InputManager::instance().initialize(defaultEmpty);
	}
	if (gameNode.hasKey("Skyboxes"))
	{
		//json::JSON skyBoxArray = gameNode["Skybox"].ArrayRange();
		int i = 0;
		for (auto& skyBox : gameNode["Skyboxes"].ArrayRange())
		{
			mSkyboxes.push_back(new Skybox());
			mSkyboxes.at(i)->initialize(skyBox);
			if (i == 0)
			{
				mMainSkybox = mSkyboxes.at(i);
			}
			i++;
		}
		RenderSystem::instance().addRenderables(mMainSkybox);
	}
	if (gameNode.hasKey("SceneModel"))
	{
		mSceneModel = new SceneModel();
		mSceneModel->initialize(gameNode["SceneModel"]);
		RenderSystem::instance().addRenderables(mSceneModel);
	}
	if (gameNode.hasKey("InstanceModel"))
	{
		mInstanceModel = new InstanceModel();
		mInstanceModel->initialize(gameNode["InstanceModel"]);
		RenderSystem::instance().addRenderables(mInstanceModel);
	}
}

void Engine::changeSkybox()
{
	if (mSBTime < 0.5f)
	{
		return;
	}
	if (!InputManager::instance().getInputState(GLFW_KEY_P))
	{
		return;
	}
	mSBTime = 0.0f;
	mSkyBoxIndex++;
	if (mSkyboxes.begin() + mSkyBoxIndex == mSkyboxes.end())
	{
		mSkyBoxIndex = 0;
	}
	RenderSystem::instance().removeRenderables(mMainSkybox);

	mMainSkybox = mSkyboxes.at(mSkyBoxIndex);

	RenderSystem::instance().addRenderables(mMainSkybox);

}

void Engine::update()
{
	while (RenderSystem::instance().getWindow() != nullptr)
	{
		float currentTime = glfwGetTime();
		float deltaTime = currentTime - lastFrameTime;
		lastFrameTime = currentTime;
		InputManager::instance().update(deltaTime);
		mSBTime += deltaTime;
		changeSkybox();
		if (mMainSkybox != nullptr)
		{
			mMainSkybox->update(deltaTime);
		}
		if (mSceneModel != nullptr)
		{
			mSceneModel->update(deltaTime);
		}
		for (auto& aOModel : mOpacityModels)
		{
			if (aOModel != nullptr)
			{
				aOModel->update(deltaTime);
			}
		}
		if (mInstanceModel != nullptr)
		{
			mInstanceModel->update(deltaTime);
		}
		RenderSystem::instance().update(deltaTime);
	}
	cleanup();
	RenderSystem::instance().cleanUp();
}
