#include "Core.h"
#include "Engine.h"

int main()
{
	Engine::instance().initialize("../Assets/SceneData.json");
	Engine::instance().update();
	return 0;
}