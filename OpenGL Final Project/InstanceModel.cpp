#include "Core.h"
#include "Shader.h"
#include "InputManager.h"
#include "InstanceModel.h"
#include "RenderSystem.h"
#include "Model.h"

InstanceModel::InstanceModel()
{
	modelMatrices = new glm::mat4[amount];
	srand(glfwGetTime());
	float radius = 10.0;
	float offset = 20.5f;
	for (unsigned int i = 0; i < amount; i++)
	{
		glm::mat4 model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0, 1, -1));

		// 1. translation: displace along circle with 'radius' in range [-offset, offset]
		float angle = (float)i / (float)amount * 360.0f;
		float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float x = sin(angle) * radius + displacement;
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float y = displacement * 0.4f; // keep height of asteroid field smaller compared to width of x and z
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float z = cos(angle) * radius + displacement;
		model = glm::translate(model, glm::vec3(x, y, z));
		//std::cout<<
		// 2. scale: Scale between 0.05 and 0.25f
		float scale = (rand() % 1) / 100.0f + 0.05;
		model = glm::scale(model, glm::vec3(scale));

		// 3. rotation: add random rotation around a (semi)randomly picked rotation axis vector
		float rotAngle = (rand() % 360);
		model = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8f));

		// 4. now add to list of matrices
		modelMatrices[i] = model;
	}
		
}

InstanceModel::~InstanceModel()
{
}

void InstanceModel::render()
{
	//IRenderable::render();
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// configure transformation matrices
	//glDepthFunc(GL_GREATER);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND); 
	glBlendFunc(GL_ONE, GL_ONE);
	glBlendEquation(GL_FUNC_ADD);
	mMainShader->use();
	mMainShader->setMat4("projection", InputManager::instance().getProjectionMatrix());
	mMainShader->setMat4("view", InputManager::instance().getViewMatrix());
	mMainShader->setInt("texture1", 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, instanceModels[currentModel]->texturesLoaded[0].Id);
		
	for (unsigned int i = 0; i < instanceModels[currentModel]->meshes.size(); i++)
	{
		glBindVertexArray(instanceModels[currentModel]->meshes[i].VAO);
		glDrawElementsInstanced(GL_TRIANGLES, instanceModels[currentModel]->meshes[i].indices.size(), GL_UNSIGNED_INT, 0, amount);
		glBindVertexArray(0);
	}
	glDisable(GL_BLEND);
}

void InstanceModel::initialize(json::JSON& instanceNode)
{
	IRenderable::initialize(instanceNode);
	
	if (instanceNode.hasKey("Models"))
	{
		int i = 0;
		for (auto& model : instanceNode["Models"].ArrayRange())
		{
			instanceModels.push_back(new Model());
			instanceModels.at(i)->loadModel(model.ToString());
			i++;
		}
		
	}
	
	unsigned int buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, amount * sizeof(glm::mat4), &modelMatrices[0], GL_STATIC_DRAW);
	loadModel();
}

void InstanceModel::update(float deltaTime)
{
	IRenderable::update(deltaTime);
	mIMTime += deltaTime;
	changeModel();
}



void InstanceModel::loadModel()
{
	for (unsigned int i = 0; i < instanceModels.at(currentModel)->meshes.size(); i++)
	{
		unsigned int VAO = instanceModels.at(currentModel)->meshes[i].VAO;
		glBindVertexArray(VAO);
		// set attribute pointers for matrix (4 times vec4)
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
		glEnableVertexAttribArray(6);
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

		glVertexAttribDivisor(3, 1);
		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		glVertexAttribDivisor(6, 1);

		glBindVertexArray(0);
	}
}

void InstanceModel::changeModel()
{
	if (mIMTime < 0.5f)
	{
		return;
	}
	if (!InputManager::instance().getInputState(GLFW_KEY_P))
	{
		return;
	}
	mIMTime = 0.0f;
	currentModel++;
	if (instanceModels.begin() + currentModel +1 == instanceModels.end())
	{
		currentModel = 0;
	}
	loadModel();
}
