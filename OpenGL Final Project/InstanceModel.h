#pragma once
#include "IRenderable.h"
class Model;
class InstanceModel : public IRenderable
{
	friend class Engine;
private:
	//float quadVertices[30] = {
	//	// positions     
	//	0.0f,  0.5f,  0.0f,  0.0f,  0.0f,
	//	0.0f, -0.5f,  0.0f,  0.0f,  1.0f,
	//	1.0f, -0.5f,  0.0f,  1.0f,  1.0f,

	//	0.0f,  0.5f,  0.0f,  0.0f,  0.0f,
	//	1.0f, -0.5f,  0.0f,  1.0f,  1.0f,
	//	1.0f,  0.5f,  0.0f,  1.0f,  0.0f
	//};

	//glm::vec3* translations;
	unsigned int amount = 1000;
	glm::mat4* modelMatrices;
	std::vector <Model*> instanceModels;
	int currentModel = 0;
	float mIMTime = 1.0f;
	//unsigned int transparentTexture;
	//unsigned int quadVAO;
protected:
	InstanceModel();
	~InstanceModel();
	// Inherited via IRenderable
	virtual void render() override;
	virtual void initialize(json::JSON&) override;
	virtual void update(float deltaTime) override;
	void loadModel();
	void changeModel();
};

