#include "Core.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "IRenderable.h"
#include "InputManager.h"
#include "RenderSystem.h"

RenderSystem::~RenderSystem()
{
	cleanUp();
}

void RenderSystem::increaseLightIntensity(float deltaTime)
{
	if (InputManager::instance().getInputState(GLFW_KEY_N))
	{
		mDirectionalLight->intensity += deltaTime;
		if (mDirectionalLight->intensity > 30.f)
		{
			mDirectionalLight->intensity = 30.f;
		}
		for (auto& pointLight : mPointLights)
		{
			pointLight->intensity += deltaTime;
			if (pointLight->intensity > 30.f)
			{
				pointLight->intensity = 30.f;
			}
		}
		for (auto& spotLight : mSpotLights)
		{
			spotLight->intensity += deltaTime;
			if (spotLight->intensity > 30.f)
			{
				spotLight->intensity = 30.f;
			}
		}
	}
	else if (InputManager::instance().getInputState(GLFW_KEY_M))
	{
		mDirectionalLight->intensity -= deltaTime;
		if (mDirectionalLight->intensity < 1.f)
		{
			mDirectionalLight->intensity = 1.f;
		}

		for (auto& pointLight : mPointLights)
		{
			pointLight->intensity -= deltaTime;
			if (pointLight->intensity < 1.f)
			{
				pointLight->intensity = 1.f;
			}
		}
		for (auto& spotLight : mSpotLights)
		{
			spotLight->intensity -= deltaTime;
			if (spotLight->intensity < 1.f)
			{
				spotLight->intensity = 1.f;
			}
		}
	}
}

void RenderSystem::changeFrameBufferSize(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, RenderSystem::instance().width, RenderSystem::instance().height);
}

void RenderSystem::initialize(json::JSON& renderSettings)
{
	if (renderSettings.hasKey("Width"))
	{
		width = renderSettings["Width"].ToInt();
	}
	if (renderSettings.hasKey("Height"))
	{
		height = renderSettings["Height"].ToInt();
	}
	if (renderSettings.hasKey("MajorVersion"))
	{
		majorVersion = renderSettings["MajorVersion"].ToInt();
	}
	if (renderSettings.hasKey("MinorVersion"))
	{
		minorVersion = renderSettings["MinorVersion"].ToInt();
	}
	if (renderSettings.hasKey("MultiSampling"))
	{
		multiSampling = renderSettings["MultiSampling"].ToInt();
	}
	if (renderSettings.hasKey("WindowName"))
	{
		windowName = renderSettings["WindowName"].ToString();
	}
	if (!glfwInit())
	{
		glfwTerminate();
		return;
	}
	glfwWindowHint(GLFW_SAMPLES, multiSampling);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, majorVersion);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minorVersion);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	mWindow = glfwCreateWindow(width, height, windowName.c_str(), nullptr, nullptr);
	if (mWindow == nullptr)
	{
		glfwTerminate();
		return;
	}

	glfwMakeContextCurrent(mWindow);
	glewExperimental = true; 

	if (glewInit() != GLEW_OK) 
	{
		glfwTerminate();
		return;
	}

	if (renderSettings.hasKey("Background Color"))
	{
		glClearColor(
			renderSettings["Background Color"]["r"].ToFloat(),
			renderSettings["Background Color"]["g"].ToFloat(),
			renderSettings["Background Color"]["b"].ToFloat(),
			renderSettings["Background Color"]["a"].ToFloat()
		);
	}
	else
	{
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	}
	glfwSetInputMode(mWindow, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glEnable(GL_DEPTH_TEST);
	glfwSetFramebufferSizeCallback(mWindow, &RenderSystem::changeFrameBufferSize);

	//Lights
	if (renderSettings.hasKey("Directional Light"))
	{
		json::JSON dLight = renderSettings["Directional Light"];
		mDirectionalLight = new DirectionalLight();
		mDirectionalLight->lightDir = glm::vec3(dLight["Direction"]["X"].ToFloat(), dLight["Direction"]["Y"].ToFloat(), dLight["Direction"]["Z"].ToFloat());
		mDirectionalLight->diffuse = glm::vec3(dLight["Diffuse"]["X"].ToFloat(), dLight["Diffuse"]["Y"].ToFloat(), dLight["Diffuse"]["Z"].ToFloat());
		mDirectionalLight->ambient = glm::vec3(dLight["Ambient"]["X"].ToFloat(), dLight["Ambient"]["Y"].ToFloat(), dLight["Ambient"]["Z"].ToFloat());
		mDirectionalLight->specular = glm::vec3(dLight["Specular"]["X"].ToFloat(), dLight["Specular"]["Y"].ToFloat(), dLight["Specular"]["Z"].ToFloat());
		mDirectionalLight->intensity = dLight["Intensity"].ToFloat();
	}

}

void RenderSystem::update(float deltaTime)
{
	if (mWindow != nullptr)
	{
		if (glfwWindowShouldClose(mWindow))
		{
			mWindow = nullptr;
			return;
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		increaseLightIntensity(deltaTime);
		for (auto& ir : rendereables)
		{
			ir->setLights(mDirectionalLight, mPointLights, mSpotLights);
			ir->render();
		}
		glfwSwapBuffers(mWindow);
	}
}

void RenderSystem::cleanUp()
{
	if (mWindow != nullptr)
	{
		glfwTerminate();
		mWindow = nullptr;
	}
	if (mDirectionalLight != nullptr)
	{
		delete mDirectionalLight;
		mDirectionalLight = nullptr;
	}
	for (auto& aPLight : mPointLights)
	{
		if (aPLight != nullptr)
		{
			delete aPLight;
			aPLight = nullptr;
		}
	}
	mPointLights.clear();
	for (auto& aSLight : mSpotLights)
	{
		if (aSLight != nullptr)
		{
			delete aSLight;
			aSLight = nullptr;
		}
	}	
	mSpotLights.clear();
}

unsigned int RenderSystem::TextureFromFile(const char* path, const char* directory)
{
	std::string filename = std::string(path);
	std::string director = std::string(directory);
	filename = director + '/' + filename;

	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format = GL_RGB;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}


unsigned int RenderSystem::loadCubemap(const std::vector<std::string>& faces)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
			);
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}
