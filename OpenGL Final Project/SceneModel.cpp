#include "Core.h"
#include "Shader.h"
#include "RenderSystem.h"
#include "InputManager.h"
#include "Model.h"
#include "SceneModel.h"

SceneModel::SceneModel()
{
	mModel = glm::mat4(1.0f);
	FRONT = glm::vec3(0.0f, 0.0f, 1.0f);
	Position = glm::vec3(0.0f, 0.0f, 0.0f);
	RIGHT = glm::normalize(glm::cross(FRONT, glm::vec3(0.0f, 1.0f, 0.0f)));
	UP = glm::normalize(glm::cross(RIGHT, FRONT));
	model = nullptr;
	speed = 10.0f;
	shininess = 1.0f;
}

SceneModel::~SceneModel()
{
	if (model != nullptr)
	{
		delete model;
		model = nullptr;
	}
}

void SceneModel::render()
{
	mMainShader->use();
	mMainShader->setFloat("material.shininess", shininess);
	if (model != nullptr)
	{
		model->Draw(mMainShader);
	}
}

void SceneModel::initialize(json::JSON& modelNode)
{
	IRenderable::initialize(modelNode);
	if (modelNode.hasKey("Model"))
	{
		model = new Model();
		model->loadModel(modelNode["Model"].ToString());
	}
	if (modelNode.hasKey("Position"))
	{
		Position = glm::vec3(modelNode["Position"]["X"].ToFloat(), modelNode["Position"]["Y"].ToFloat(), modelNode["Position"]["Z"].ToFloat());
		mModel = glm::translate(mModel, Position);
	}
	if (modelNode.hasKey("Rotation"))
	{
		float aval = modelNode["Rotation"]["X"].ToFloat();
		mModel = glm::rotate(mModel, glm::radians(aval), glm::vec3(1, 0, 0));
		aval = modelNode["Rotation"]["Y"].ToFloat();
		mModel = glm::rotate(mModel, glm::radians(aval), glm::vec3(0, 1, 0));
		aval = modelNode["Rotation"]["Z"].ToFloat();
		mModel = glm::rotate(mModel, glm::radians(aval), glm::vec3(0, 0, 1));
	}

	if (modelNode.hasKey("Scale"))
	{
		mModel = glm::scale(mModel, glm::vec3(modelNode["Scale"]["X"].ToFloat(), modelNode["Scale"]["Y"].ToFloat(), modelNode["Scale"]["Z"].ToFloat()));
	}
	if (modelNode.hasKey("Speed"))
	{
		speed = modelNode["Speed"].ToFloat();
	}
	if (modelNode.hasKey("Shininess"))
	{
		shininess = modelNode["Shininess"].ToFloat();
	}
}

void SceneModel::update(float deltaTime)
{
	IRenderable::update(deltaTime);
	HandleInput(speed * deltaTime);
	mMainShader->use();
	if (InputManager::instance().getInputState(GLFW_KEY_Q))
	{
		mMainShader->setFloat("time", deltaTime);
	}
	else
	{
		mMainShader->setFloat("time", glfwGetTime());
	}
	mMainShader->setMat4("model", mModel);
	mMainShader->setMat4("projection", InputManager::instance().getProjectionMatrix());
	mMainShader->setMat4("view", InputManager::instance().getViewMatrix());
}

void SceneModel::HandleInput(float deltaTime)
{
	glm::vec3 aTempPos = Position;
	float aRot = 0.0f;
	if (InputManager::instance().getInputState(GLFW_KEY_UP))
	{
		aTempPos += FRONT * deltaTime;
	}
	if (InputManager::instance().getInputState(GLFW_KEY_DOWN))
	{
		aTempPos -= FRONT * deltaTime;
	}
	if (InputManager::instance().getInputState(GLFW_KEY_LEFT))
	{
		//aTempPos -= RIGHT * deltaTime;
		aRot = deltaTime;
	}
	if (InputManager::instance().getInputState(GLFW_KEY_RIGHT))
	{
		//aTempPos += RIGHT * deltaTime;
		aRot = -deltaTime;
	}

	if (aTempPos != Position)
	{
		glm::vec3 aMovementVector = aTempPos - Position;
		mModel = glm::translate(mModel, aMovementVector);
		Position = aTempPos;
		for (auto& aSLight : spotLights)
		{
			if (aSLight != nullptr)
			{
				aSLight->lightPos += aMovementVector;
			}
		}
	}
	if (aRot != 0.0f)
	{
		mModel = glm::rotate(mModel, glm::radians(aRot), UP);
		float aCos = glm::cos(glm::radians(aRot));
		float aSin = glm::sin(glm::radians(aRot));
		FRONT.x = FRONT.x * aSin;
		FRONT.z = FRONT.z * aCos;
		FRONT = glm::normalize(FRONT);
		RIGHT = glm::normalize(glm::cross(FRONT, glm::vec3(0.0f, 1.0f, 0.0f)));
		UP = glm::normalize(glm::cross(RIGHT, FRONT));
	}
}
