#pragma once
#ifndef _ENGINE_H_
#define _ENGINE_H_
class Skybox;
class OpacityModel;
class SceneModel;
class InstanceModel;
class Engine
{
	float lastFrameTime = 0.0f;
	std::vector<Skybox*> mSkyboxes;
	Skybox* mMainSkybox = nullptr;
	std::list<OpacityModel*> mOpacityModels;
	SceneModel* mSceneModel = nullptr;
	InstanceModel* mInstanceModel = nullptr;
	int mSkyBoxIndex = 0;
	float mSBTime = 1.0f;
private:
	Engine() = default;
	~Engine();
	Engine(const Engine&) = default;
	Engine& operator= (const Engine&) = default;
	void cleanup();
	void changeSkybox();
public:
	static Engine& instance()
	{
		static Engine _instance;
		return _instance;
	}

	void initialize(std::string sceneDataPath);
	void update();
};

#endif