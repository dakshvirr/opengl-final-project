#pragma once
#include "IRenderable.h"
class Skybox : public IRenderable
{
	friend class Engine;
	unsigned int vao;
	unsigned int cubemap;
protected:
	// Inherited via IRenderable
	virtual void render() override;
	virtual void initialize(json::JSON&) override;
	virtual void update(float deltaTime) override;
};

