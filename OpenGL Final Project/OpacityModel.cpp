#include "Core.h"
#include "Shader.h"
#include "RenderSystem.h"
#include "InputManager.h"
#include "OpacityModel.h"

void OpacityModel::render()
{
}

void OpacityModel::initialize(json::JSON& opacityNode)
{
	IRenderable::initialize(opacityNode);
}

void OpacityModel::update(float deltaTime)
{
	IRenderable::update(deltaTime);
	mMainShader->use();
	mMainShader->setMat4("projection", InputManager::instance().getProjectionMatrix());
	mMainShader->setMat4("view", InputManager::instance().getViewMatrix());
}
