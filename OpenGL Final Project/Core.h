#pragma once
#ifndef _CORE_H_
#define _CORE_H_

#include <list>
#include <map>
#include <vector>
#include <iostream>

#include <string>
#include <fstream>
#include <sstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>


#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "json.hpp"

#endif