#pragma once
#ifndef _RENDERSYSTEM_H_
#define _RENDERSYSTEM_H_

#include "ISystem.h"

struct DirectionalLight
{
	glm::vec3 lightDir;
	glm::vec3 diffuse;
	glm::vec3 ambient;
	glm::vec3 specular;
	float intensity;
};
struct PointLight 
{
	glm::vec3 lightPos;
	float constant;
	float linear;
	float quadratic;
	glm::vec3 diffuse;
	glm::vec3 ambient;
	glm::vec3 specular;
	float intensity;
};
struct SpotLight 
{
	glm::vec3 lightPos;
	glm::vec3 lightDir;
	float cutOff;
	float outerCutOff;
	float constant;
	float linear;
	float quadratic;
	glm::vec3 diffuse;
	glm::vec3 ambient;
	glm::vec3 specular;
	float intensity;
};

class IRenderable;

class RenderSystem : public ISystem
{
	friend class InputManager;
	friend class Engine;
private:
	int width = 1280;
	int height = 720;
	int majorVersion = 3;
	int minorVersion = 3;
	int multiSampling = 4;
	std::string windowName = "OpenGL";
	GLFWwindow* mWindow = nullptr;
	std::list<IRenderable*> rendereables;
	DirectionalLight* mDirectionalLight = nullptr;
	std::list<PointLight*> mPointLights;
	std::list<SpotLight*> mSpotLights;
private:
	RenderSystem() = default;
	~RenderSystem();
	RenderSystem(const RenderSystem&) = default;
	RenderSystem& operator= (const RenderSystem&) = default;
	void increaseLightIntensity(float deltaTime);
public:
	static RenderSystem& instance()
	{
		static RenderSystem _instance;
		return _instance;
	}

	unsigned int TextureFromFile(const char* path, const char* directory);
	unsigned int loadCubemap(const std::vector<std::string>& faces);
	inline const int& getWindowHeight() { return height; }
	inline const int& getWindowWidth() { return width; }
	static void changeFrameBufferSize(GLFWwindow* window, int w, int h);
	void addPointLight(PointLight* pLight) { mPointLights.push_back(pLight); }
	void addSpotLight(SpotLight* pLight) { mSpotLights.push_back(pLight); }
protected:
	// Inherited via ISystem
	virtual void initialize(json::JSON&) override;
	virtual void update(float deltaTime) override;
	void addRenderables(IRenderable* renderable) { rendereables.push_back(renderable); }
	void removeRenderables(IRenderable* renderable) { rendereables.remove(renderable); }
	inline GLFWwindow* getWindow() { return mWindow; }
	void cleanUp();
};

#endif