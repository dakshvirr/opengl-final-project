#pragma once
#include "IRenderable.h"
class OpacityModel : public IRenderable
{
	friend class Engine;
protected:
	// Inherited via IRenderable
	virtual void render() override;
	virtual void initialize(json::JSON&) override;
	virtual void update(float deltaTime) override;
};

