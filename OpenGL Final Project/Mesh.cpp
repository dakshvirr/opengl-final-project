#include "Core.h"
#include "Shader.h"
#include "Mesh.h"

void Mesh::Draw(Shader* const shader)
{
	unsigned int diffuseTexNum = 1;
	unsigned int specularTexNum = 1;
	unsigned int normalTexNum = 1;
	unsigned int heightTexNum = 1;

	for (unsigned int i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i); //activation for texture unit 

		std::string number; //to retrieve texture 
		std::string name = textures[i].type;
		if (name == "texture_diffuse")
			number = std::to_string(diffuseTexNum++); //increment diffuseTexNum
		else 	if (name == "texture_specular")
			number = std::to_string(specularTexNum++);
		else 	if (name == "texture_normal")
			number = std::to_string(normalTexNum++);
		else 	if (name == "texture_height")
			number = std::to_string(heightTexNum++);

		shader->setInt((name + number).c_str(), i);

		//bind texture
		glBindTexture(GL_TEXTURE_2D, textures[i].Id);
	}

	//Now let's draw the mesh
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

}

void Mesh::setupMesh()
{
	// create buffers/arrays
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	// load data into vertex buffers
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);


	//Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

	//Vertexnormals
	glEnableVertexAttribArray(1);
	//notice how we use 'offsetof' to get the stride (i.e. offset) to get to the 'Normal' data
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));

	// vertex texture coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

	// vertex tangent
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));

	// vertex bitangent
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));

	glBindVertexArray(0);

}

Mesh::Mesh(std::vector<Vertex> verts, std::vector<unsigned int> inds, std::vector<Texture> texts)
{
	vertices = verts;
	indices = inds;
	textures = texts;
}