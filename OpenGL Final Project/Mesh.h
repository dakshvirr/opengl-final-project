#pragma once
#ifndef _MESH_H_
#define _MESH_H_

struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Tangent;
	glm::vec3 Bitangent;


};

struct Texture
{
	unsigned int Id;
	std::string type;
	std::string path;
};

class Shader;

class Mesh
{
	friend class Model;
	friend class InstanceModel;
protected:

	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector <Texture> textures;
	unsigned int VAO;

	void Draw(Shader* const shader);
	void setupMesh();
	void setupInstanceMesh() {};
private:

	unsigned int VBO, EBO;

public:
	Mesh(std::vector<Vertex> verts, std::vector<unsigned int> inds, std::vector<Texture> texts);
};

#endif