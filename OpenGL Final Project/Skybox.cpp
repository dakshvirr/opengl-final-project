#include "Core.h"
#include "Shader.h"
#include "RenderSystem.h"
#include "InputManager.h"
#include "Skybox.h"


void Skybox::render()
{
	mMainShader->use();
	glDepthFunc(GL_LEQUAL);
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap);
	mMainShader->setInt("skybox", 0);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glDepthFunc(GL_LESS);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
}

void Skybox::initialize(json::JSON& skyboxNode)
{
	IRenderable::initialize(skyboxNode);
	mMainShader->use();
	GLfloat skyboxVertices[] = { -2000.0f,  2000.0f, -2000.0f,		-2000.0f, -2000.0f, -2000.0f,		 2000.0f, -2000.0f, -2000.0f,		 2000.0f, -2000.0f, -2000.0f,
		 2000.0f,  2000.0f, -2000.0f,		-2000.0f,  2000.0f, -2000.0f,		-2000.0f, -2000.0f,  2000.0f,		-2000.0f, -2000.0f, -2000.0f,		-2000.0f,  2000.0f, -2000.0f,
		-2000.0f,  2000.0f, -2000.0f,		-2000.0f,  2000.0f,  2000.0f,		-2000.0f, -2000.0f,  2000.0f,		 2000.0f, -2000.0f, -2000.0f,		 2000.0f, -2000.0f,  2000.0f,
		 2000.0f,  2000.0f,  2000.0f,		 2000.0f,  2000.0f,  2000.0f,		 2000.0f,  2000.0f, -2000.0f,		 2000.0f, -2000.0f, -2000.0f,		-2000.0f, -2000.0f,  2000.0f,
		-2000.0f,  2000.0f,  2000.0f,		 2000.0f,  2000.0f,  2000.0f,		 2000.0f,  2000.0f,  2000.0f,		 2000.0f, -2000.0f,  2000.0f,		-2000.0f, -2000.0f,  2000.0f,
		-2000.0f,  2000.0f, -2000.0f,		 2000.0f,  2000.0f, -2000.0f,		 2000.0f,  2000.0f,  2000.0f,		 2000.0f,  2000.0f,  2000.0f,		-2000.0f,  2000.0f,  2000.0f,
		-2000.0f,  2000.0f, -2000.0f,		-2000.0f, -2000.0f, -2000.0f,		-2000.0f, -2000.0f,  2000.0f,		 2000.0f, -2000.0f, -2000.0f,		 2000.0f, -2000.0f, -2000.0f,
		-2000.0f, -2000.0f,  2000.0f,		 2000.0f, -2000.0f,  2000.0f	};
	GLuint vertexBuffer;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vertexBuffer);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	std::vector<std::string> faces
	{
		skyboxNode["Right"].ToString(),
		skyboxNode["Left"].ToString(),
		skyboxNode["Top"].ToString(),
		skyboxNode["Bottom"].ToString(),
		skyboxNode["Front"].ToString(),
		skyboxNode["Back"].ToString()
	};
	cubemap = RenderSystem::instance().loadCubemap(faces);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap);
	mMainShader->setInt("skybox", 0);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
}

void Skybox::update(float deltaTime)
{
	IRenderable::update(deltaTime);
	mMainShader->use();
	mMainShader->setMat4("projection", InputManager::instance().getProjectionMatrix());
	mMainShader->setMat4("view", glm::mat4(glm::mat3(InputManager::instance().getViewMatrix())));
}
