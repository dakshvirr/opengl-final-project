#include "Core.h"
#include "Model.h"
#include "RenderSystem.h"

Model::Model(bool gamma)
	:gammaCorrection(gamma)
{

}

bool Model::loadModel(std::string const& path)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	//using a single '&' below for bitwise calc
	if (!scene || (scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE) || !scene->mRootNode)
	{
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
		return false;
	}

	//retreive the directory path of the file:
	directory = path.substr(0, path.find_last_of('/'));

	//process the ASSIMP root node recursively
	processNode(scene->mRootNode, scene);
	return true;
}

void Model::Draw(Shader* const shader)
{
	for (unsigned int i = 0; i < meshes.size(); i++)
	{
		meshes[i].Draw(shader);
	}
}

void Model::processNode(aiNode* node, const aiScene* scene)
{
	for (unsigned int i = 0; i < node->mNumMeshes; i++)
	{
		// the node object only contains indices to index the actual objects in the scene. 
		// the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		Mesh aMesh = processMesh(mesh, scene);
		aMesh.setupMesh();
		meshes.push_back(aMesh);
	}
	// after we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for (unsigned int i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}
}

Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
	// data to fill
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Texture> textures;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		glm::vec3 aVector; 
		aVector.x = mesh->mVertices[i].x;
		aVector.y = mesh->mVertices[i].y;
		aVector.z = mesh->mVertices[i].z;
		vertex.Position = aVector;

		if (mesh->HasNormals())
		{
			aVector.x = mesh->mNormals[i].x;
			aVector.y = mesh->mNormals[i].y;
			aVector.z = mesh->mNormals[i].z;
			vertex.Normal = aVector;
		}
		else
		{
			vertex.Normal = glm::vec3();
		}
		

		//texture coords
		//does the mesh contain tex coord data?
		if (mesh->mTextureCoords[0])
		{
			glm::vec2 aVec2; //placeholder to convert assimp's vec2 to glm's vec2 type
			aVec2.x = mesh->mTextureCoords[0][i].x;
			aVec2.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = aVec2;

		}
		else
		{
			vertex.TexCoords = glm::vec2();
		}

		if (mesh->HasTangentsAndBitangents())
		{
			aVector.x = mesh->mTangents[i].x;
			aVector.y = mesh->mTangents[i].y;
			aVector.z = mesh->mTangents[i].z;
			vertex.Tangent = aVector;

			aVector.x = mesh->mBitangents[i].x;
			aVector.y = mesh->mBitangents[i].y;
			aVector.z = mesh->mBitangents[i].z;
			vertex.Bitangent = aVector;
		}
		else
		{
			vertex.Tangent = glm::vec3();
			vertex.Bitangent = glm::vec3();
		}

		//push the vertext to the vertices
		vertices.push_back(vertex);
	}

	//go through each of the mesh's faces and get the vertex indices
	for (int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		//retrieve all indices of the face and store them in our indices vector
		for (int j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);

	}


	//process materials:
	aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

	std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
	textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

	std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
	textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

	std::vector<Texture> normalMaps = loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
	textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());

	std::vector<Texture> heightMaps = loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
	textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());


	return Mesh(vertices, indices, textures);
}

std::vector<Texture> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		
		bool skip = false;
		for (unsigned int j = 0; j < texturesLoaded.size(); j++)
		{
			if (std::strcmp(texturesLoaded[j].path.data(), str.C_Str()) == 0)
			{
				textures.push_back(texturesLoaded[j]);
				skip = true; 
				break;
			}
		}
		if (!skip)
		{   
			Texture texture;
			texture.Id = RenderSystem::instance().TextureFromFile(str.C_Str(),directory.c_str());
			texture.type = typeName;
			texture.path = str.C_Str();
			textures.push_back(texture);
			texturesLoaded.push_back(texture);  
		}
	}
	return textures;
}
