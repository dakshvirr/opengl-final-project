PROG54310 Final Project Readme

Created By:
Dakshvir Singh Rehill
Prashat Gajre

Folder Hierarchy:

Build/bin has all the builds needed to run the project
Build/assets has all the assets

OpenGL Final Project/ is the Project Folder
OpenGL Final Project.sln is the solution file

There are some configurations done to ensure that all intermediate files are in the intermediate folder and build is in the build/bin folder.
Please use the solution to test and run the project.
You can directly run any of the builds as well.

Renderables:
There are 4 skyboxes
There are 3 Instance Models
There is one spaceship

Lights:
There are 4 point lights and one directional light

Shaders:

The skybox uses 1 shader (uses cubemaps)
The spaceship uses 3 shaders (uses geometry multi lighting and animation)
The instance model uses 1 shader (uses instance , opacity and blending)

Controls:

Use WSAD to move the camera, scroller to change FOV and mouse movement to change the view
Use Arrow Keys to move the spaceship

Shader Controls:

Press 0 to activate the simple shader that uses no lights
Press 1 to activate the multi lighting shader.  Press N to increase the light intensity and Press M to decrease the light intensity
Press 2 to activate the geometry shader implementation with the animation of the spaceship exploding. Press and hold Q to examine the exploded spaceship (without animation).

Skybox and Instance Controls:

Press P to change the skybox and instanced models.