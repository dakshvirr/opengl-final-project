

#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 aTexCoords;
layout (location = 3) in mat4 aInstanceMatrix;

out vec2 TexCoords;
out vec4 color;
uniform mat4 projection;
uniform mat4 view;

void main()
{
	color = vec4(1.0f,1.0f,1.0f,0.3f);
    TexCoords = aTexCoords;
    gl_Position = projection * view * aInstanceMatrix * vec4(aPos, 1.0f); 
}

