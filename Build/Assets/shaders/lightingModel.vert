#version 330 core


layout(location =0) in vec3 pos;
layout(location =1) in vec3 normal;
layout(location =2)  in vec2 texCoords;


out vec2 UV;
out vec3 FragPos;
out vec3 NormalCamPos;
out vec3 EyeDir;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
   UV = texCoords;
   FragPos = (model * vec4(pos,1.0f)).xyz;
   vec3 VertCamPos = (view * model * vec4(pos,1.0f)).xyz;
   EyeDir = vec3(0.0f,0.0f,0.0f) - VertCamPos;
   NormalCamPos = mat3(transpose(inverse(model))) * normal;
   gl_Position = projection * view * vec4(FragPos,1.0f);

}