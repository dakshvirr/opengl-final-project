

#version 330 core
out vec4 FragColor;

in vec2 TexCoords;
in vec4 color;

uniform sampler2D texture1;

void main()
{
    FragColor = texture(texture1, TexCoords) * color;
}

