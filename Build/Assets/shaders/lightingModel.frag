#version 330 core

struct Material
{
	sampler2D diffuseMap;
	sampler2D specularMap;
	sampler2D normalMap;
	sampler2D heightMap;
	float shininess;
};

struct DirectionalLight
{
	vec3 lightDir;
	vec3 diffuse;
	vec3 ambient;
	vec3 specular;
	float intensity;
};

struct PointLight
{
	vec3 lightPos;
	float constant;
	float linear;
	float quadratic;
	vec3 diffuse;
	vec3 ambient;
	vec3 specular;
	float intensity;
};

struct SpotLight
{
	vec3 lightPos;
	vec3 lightDir;
	float cutOff;
	float outerCutOff;
	float constant;
	float linear;
	float quadratic;
	vec3 diffuse;
	vec3 ambient;
	vec3 specular;
	float intensity;
};

out vec3 color;

in vec2 UV;
in vec3 FragPos;
in vec3 NormalCamPos;
in vec3 EyeDir;

uniform Material material;
uniform DirectionalLight directionalLight;

#define NO_OF_POINT_LIGHTS  50
#define NO_OF_SPOT_LIGHTS  50

uniform int noOfPointLights;
uniform int noOfSpotLights;
uniform PointLight pointLights[NO_OF_POINT_LIGHTS];
uniform SpotLight spotLights[NO_OF_SPOT_LIGHTS];

vec3 CalcDirLight(DirectionalLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.lightDir);
	float diff = max(dot(normal,lightDir),0.0);
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir),0.0), material.shininess);
	vec3 ambient = light.ambient * vec3(texture(material.diffuseMap,UV));
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuseMap,UV));
	vec3 sp = vec3(texture(material.specularMap,UV));
	if(sp.x == 0)
	{
		sp = vec3(1.0f,1.0f,1.0f);
	}
	vec3 specular = light.specular * spec * sp;
	return (ambient + diffuse + specular) * light.intensity;
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.lightPos - fragPos);
	float diff = max(dot(normal,lightDir),0.0);
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir,reflectDir),0.0),material.shininess);
	float distance = length(light.lightPos - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	vec3 ambient = light.ambient * vec3(texture(material.diffuseMap, UV));
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuseMap,UV));
	vec3 sp = vec3(texture(material.specularMap,UV));
	if(sp.x == 0)
	{
		sp = vec3(1.0f,1.0f,1.0f);
	}
	vec3 specular = light.specular * spec * sp;
	ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular) * light.intensity;
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.lightPos - fragPos);
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	float distance = length(light.lightPos - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	float theta = dot(lightDir, normalize(-light.lightDir)); 
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
	vec3 ambient = light.ambient * vec3(texture(material.diffuseMap, UV));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuseMap, UV));
    vec3 sp = vec3(texture(material.specularMap,UV));
	if(sp.x == 0)
	{
		sp = vec3(1.0f,1.0f,1.0f);
	}
	vec3 specular = light.specular * spec * sp;
	ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;
    return (ambient + diffuse + specular) * light.intensity;
}

void main()
{
	vec3 norm = normalize(vec3(texture(material.normalMap,UV)) * NormalCamPos);
	vec3 result = CalcDirLight(directionalLight, norm, EyeDir);
	for(int i = 0; i < noOfPointLights; i++)
	{
		result += CalcPointLight(pointLights[i],norm,FragPos, EyeDir);
	}
	for(int i = 0; i < noOfSpotLights; i++)
	{
		result += CalcSpotLight(spotLights[i],norm,FragPos, EyeDir);
	}	
	color = result;
}
